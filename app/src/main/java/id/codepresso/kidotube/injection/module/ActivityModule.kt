package id.codepresso.kidotube.injection.module

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import id.codepresso.kidotube.injection.ActivityContext

@Module
class ActivityModule(private val mActivity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return mActivity
    }

    @Provides
    @ActivityContext
    fun providesContext(): Context {
        return mActivity
    }
}