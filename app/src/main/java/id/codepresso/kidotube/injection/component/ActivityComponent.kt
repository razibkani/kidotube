package id.codepresso.kidotube.injection.component

import dagger.Component
import id.codepresso.kidotube.injection.PerActivity
import id.codepresso.kidotube.injection.module.ActivityModule
import id.codepresso.kidotube.ui.homepage.HomepageActivity
import id.codepresso.kidotube.ui.video.channels.list_channels.ListChannelFragment
import id.codepresso.kidotube.ui.video.channels.list_playlist.ListPlaylistFragment
import id.codepresso.kidotube.ui.video.detail_video.VideoDetailActivity
import id.codepresso.kidotube.ui.video.favorite_video.FavoriteVideoFragment
import id.codepresso.kidotube.ui.video.list_video.VideoListFragment
import id.codepresso.kidotube.ui.video.recommendation.VideoRecommendationFragment

@PerActivity
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(homepageActivity: HomepageActivity)
    fun inject(videoDetailActivity: VideoDetailActivity)

    fun inject(videoRecommendationFragment: VideoRecommendationFragment)
    fun inject(videoListFragment: VideoListFragment)
    fun inject(listChannelFragment: ListChannelFragment)
    fun inject(listPlaylistFragment: ListPlaylistFragment)
    fun inject(favoriteVideoListFragment: FavoriteVideoFragment)
}