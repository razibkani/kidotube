package id.codepresso.kidotube.injection.component

import android.content.Context
import dagger.Component
import id.codepresso.kidotube.KidoTubeApp
import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.injection.ApplicationContext
import id.codepresso.kidotube.injection.module.ApplicationModule
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(application: KidoTubeApp)

    @ApplicationContext
    fun context(): Context

    fun dataManager(): DataManager
    fun adsHelper(): AdsHelper
    fun schedulers(): SchedulerProvider
}