package id.codepresso.kidotube.injection.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.data.local.Database
import id.codepresso.kidotube.data.local.PreferencesHelper
import id.codepresso.kidotube.data.remote.ApiService
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.injection.ApplicationContext
import id.codepresso.kidotube.util.Config
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Singleton


@Module
class ApplicationModule(private val mApplication: Application) {

    @Provides
    fun provideApplication(): Application {
        return mApplication
    }

    @Provides
    @ApplicationContext
    fun provideContext(): Context {
        return mApplication
    }

    @Provides
    fun provideSchedulers(): SchedulerProvider {
        return SchedulerProvider()
    }

    @Provides
    fun provideApiService(): ApiService {
        return ApiService.create()
    }

    @Provides
    @Singleton
    fun providePreferencesHelper(): PreferencesHelper {
        return PreferencesHelper(mApplication)
    }

    @Provides
    @Singleton
    fun provideDatabase(): Database {
        return Room.databaseBuilder(mApplication, Database::class.java, Config.DB_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build()
    }

    @Provides
    @Singleton
    fun provideDataManager(): DataManager {
        return DataManager(provideApiService(), providePreferencesHelper(), provideDatabase())
    }

    @Provides
    @Singleton
    fun provideAdsHelper(): AdsHelper {
        return AdsHelper(providePreferencesHelper())
    }
}