package id.codepresso.kidotube.injection

import javax.inject.Qualifier

@Qualifier
@Retention(value = AnnotationRetention.RUNTIME)
annotation class ActivityContext