package id.codepresso.kidotube

import android.app.Application
import android.content.Context
import com.crashlytics.android.Crashlytics
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.injection.component.ApplicationComponent
import id.codepresso.kidotube.injection.component.DaggerApplicationComponent
import id.codepresso.kidotube.injection.module.ApplicationModule
import io.fabric.sdk.android.Fabric

class KidoTubeApp : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this)).build()
        applicationComponent.inject(this)

        AdsHelper.initAds(this)

        Fabric.with(this, Crashlytics())
    }

    companion object {
        fun get(context: Context): KidoTubeApp {
            return context.applicationContext as KidoTubeApp
        }
    }

    fun getComponent(): ApplicationComponent {
        return applicationComponent
    }
}