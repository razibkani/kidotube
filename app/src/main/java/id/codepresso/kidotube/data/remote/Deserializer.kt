package id.codepresso.kidotube.data.remote

import com.google.gson.*
import id.codepresso.kidotube.data.model.VideoListResponse
import java.lang.reflect.Type

object Deserializer {

    class VideoListDeserializer : JsonDeserializer<VideoListResponse> {
        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): VideoListResponse {
            val videoListResponse = Gson().fromJson<VideoListResponse>(json, VideoListResponse::class.java)
            val jsonObject = json.asJsonObject

            if (jsonObject.has("items")) {
                val items = jsonObject.getAsJsonArray("items")
                for (i in 0..(items.size() - 1)) {
                    val id = items.get(i).asJsonObject.get("id")
                    if (id != null && !id.isJsonNull) {
                        if (id.isJsonPrimitive) {
                            videoListResponse.items[i].videoId
                        } else {
                            val objectId = id.asJsonObject.get("videoId")
                            videoListResponse.items[i].videoId = objectId.asString
                        }
                    }
                }
            }

            return videoListResponse
        }
    }
}