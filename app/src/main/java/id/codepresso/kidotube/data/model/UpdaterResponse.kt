package id.codepresso.kidotube.data.model
import com.google.gson.annotations.SerializedName

data class UpdaterResponse(
    @SerializedName("version") val version: Version
)

data class Version(
    @SerializedName("version_code") val versionCode: Int,
    @SerializedName("update_message") val updateMessage: String,
    @SerializedName("video_category_version") val videoCategoryVersion: Int,
    @SerializedName("quran_version") val quranVersion: Int,
    @SerializedName("radio_version") val radioVersion: Int
)