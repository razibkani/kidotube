package id.codepresso.kidotube.data

import id.codepresso.kidotube.data.local.Database
import id.codepresso.kidotube.data.local.PreferencesHelper
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.data.model.PlaylistListResponse
import id.codepresso.kidotube.data.model.UpdaterResponse
import id.codepresso.kidotube.data.model.VideoListResponse
import id.codepresso.kidotube.data.remote.ApiService
import id.codepresso.kidotube.util.Config
import io.reactivex.Flowable

class DataManager(private val apiService: ApiService,
                  private val prefs: PreferencesHelper,
                  private val database: Database) {

    fun getVideoCategory(): Flowable<List<VideoCategoryItem>> {
        return if (prefs.getBoolean(PreferencesHelper.Key.VIDEO_CATEGORY_MUST_UPDATE, true)) {

            prefs.setBoolean(PreferencesHelper.Key.VIDEO_CATEGORY_MUST_UPDATE, false)

            apiService.getVideoCategory().map {
                for (item in it.data) {
                    database.videoCategoryDao().insert(item)
                }
                it.data
            }
        } else {
            database.videoCategoryDao().getVideoCategory()
        }
    }

    fun getVideosByPlaylistId(maxResult: Int, playlistId: String): Flowable<VideoListResponse> {
        return apiService.getVideosByPlaylistId("snippet", maxResult, playlistId, Config.YOUTUBE_API_KEY)
    }

    fun getUpdater(): Flowable<UpdaterResponse> {
        return apiService.getUpdater()
    }

    fun getNextPageVideosByPlaylistId(maxResult: Int, playlistId: String, pageToken: String): Flowable<VideoListResponse> {
        return apiService.getNextPageVideosByPlaylistId("snippet", maxResult, playlistId, pageToken, Config.YOUTUBE_API_KEY)
    }

    fun getChannels(ids: String): Flowable<List<ChannelItem>> {
        return if (prefs.getBoolean(PreferencesHelper.Key.VIDEO_CHANNEL_MUST_UPDATE, true)) {

            apiService.getChannels("snippet,statistics", ids, Config.YOUTUBE_API_KEY)
                    .map {
                        if (it.items.isNotEmpty()) {
                            prefs.setBoolean(PreferencesHelper.Key.VIDEO_CHANNEL_MUST_UPDATE, false)

                            for (item in it.items) {
                                database.channelItemDao().insert(item)
                            }
                        }
                        it.items
                    }
        } else {
            database.channelItemDao().getChannels()
        }
    }

    fun resetChannelUpdateState() {
        prefs.setBoolean(PreferencesHelper.Key.VIDEO_CHANNEL_MUST_UPDATE, true)
    }

    fun getVideosByChannelId(channelId: String, maxResult: Int): Flowable<VideoListResponse> {
        return apiService.getVideosByChannelId("snippet,id", maxResult, channelId,
                "date", "video", Config.YOUTUBE_API_KEY)
    }

    fun getNextPageVideosByChannelId(channelId: String, maxResult: Int, pageToken: String): Flowable<VideoListResponse> {
        return apiService.getNextPageVideosByChannelId("snippet,id", maxResult, channelId,
                pageToken, "date", "video", Config.YOUTUBE_API_KEY)
    }

    fun getPlaylistByChannelId(maxResult: Int, channelId: String): Flowable<PlaylistListResponse> {
        return apiService.getPlaylistByChannelId("snippet,contentDetails", maxResult, channelId, Config.YOUTUBE_API_KEY)
    }

    fun getNextPagePlaylistByChannelId(maxResult: Int, channelId: String, pageToken: String): Flowable<PlaylistListResponse> {
        return apiService.getNextPagePlaylistByChannelId("snippet,contentDetails", maxResult, channelId, pageToken, Config.YOUTUBE_API_KEY)
    }

    fun saveLatestVideoPosition(videoId: String, position: Float) {
        prefs.setFloat(String.format(PreferencesHelper.Key.VIDEO_POSITION, videoId), position)
    }

    fun getLatestVideoPosition(videoId: String): Float {
        return prefs.getFloat(String.format(PreferencesHelper.Key.VIDEO_POSITION, videoId))
    }

    fun setUpdateState(newVideoCategoryVersion: Int) {
        val currentVideoVersion = prefs.getInt(PreferencesHelper.Key.VIDEO_CATEGORY_VERSION)

        if (newVideoCategoryVersion > currentVideoVersion) {
            prefs.setInt(PreferencesHelper.Key.VIDEO_CATEGORY_VERSION, newVideoCategoryVersion)
            prefs.setBoolean(PreferencesHelper.Key.VIDEO_CATEGORY_MUST_UPDATE, true)
        }
    }

    fun saveFavoriteVideo(videoItem: VideoItem) {
        database.videoItemDao().insert(videoItem)
    }

    fun removeFavoriteVideo(videoItem: VideoItem) {
        database.videoItemDao().delete(videoItem)
    }

    fun getFavoriteVideosFlowable(): Flowable<List<VideoItem>> {
        return database.videoItemDao().getVideosFlowable()
    }

    fun getFavoriteVideos(): List<VideoItem> {
        return database.videoItemDao().getVideos()
    }
}