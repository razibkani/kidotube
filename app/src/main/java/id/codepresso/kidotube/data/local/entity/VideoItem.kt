package id.codepresso.kidotube.data.local.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "video_item")
@Parcelize
data class VideoItem(
        @SerializedName("kind") val kind: String,
        @SerializedName("etag") val etag: String,
        @PrimaryKey var videoId: String,
        @Embedded @SerializedName("snippet") val snippet: Snippet
) : Parcelable

@Parcelize
data class Snippet(
        @SerializedName("publishedAt") val publishedAt: String,
        @SerializedName("channelId") val channelId: String,
        @SerializedName("title") val title: String,
        @SerializedName("description") val description: String,
        @Embedded @SerializedName("thumbnails") val thumbnails: Thumbnails,
        @SerializedName("channelTitle") val channelTitle: String,
        @SerializedName("playlistId") val playlistId: String?,
        @SerializedName("position") val position: Int,
        @Embedded @SerializedName("resourceId") val resourceId: ResourceId?
) : Parcelable

@Parcelize
data class ResourceId(
        @ColumnInfo(name = "resource_kind")
        @SerializedName("kind") val kind: String,
        @ColumnInfo(name = "resource_video_id")
        @SerializedName("videoId") val videoId: String
) : Parcelable

@Parcelize
data class Thumbnails(
        @Embedded @SerializedName("medium") val medium: Medium
) : Parcelable

@Parcelize
data class Medium(
        @SerializedName("url") val url: String,
        @SerializedName("width") val width: Int,
        @SerializedName("height") val height: Int
) : Parcelable