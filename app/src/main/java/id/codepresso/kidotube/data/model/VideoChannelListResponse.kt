package id.codepresso.kidotube.data.model
import com.google.gson.annotations.SerializedName
import id.codepresso.kidotube.data.local.entity.ChannelItem

data class VideoChannelListResponse(
        @SerializedName("kind") val kind: String,
        @SerializedName("etag") val etag: String,
        @SerializedName("items") val items: List<ChannelItem>
)