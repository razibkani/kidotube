package id.codepresso.kidotube.data.local.dao

import android.arch.persistence.room.*
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import io.reactivex.Flowable

@Dao
interface VideoCategoryDao {

    @Query("SELECT * FROM video_category")
    fun getVideoCategory(): Flowable<List<VideoCategoryItem>>

    @Insert
    fun insert(vararg videoCategory: VideoCategoryItem)

    @Update
    fun update(vararg videoCategory: VideoCategoryItem)

    @Delete
    fun delete(videoCategory: VideoCategoryItem)

}