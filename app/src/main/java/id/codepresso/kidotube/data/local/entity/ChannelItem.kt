package id.codepresso.kidotube.data.local.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "channel_item")
@Parcelize
data class ChannelItem(
        @PrimaryKey @SerializedName("id") val id: String,
        @SerializedName("kind") val kind: String,
        @SerializedName("etag") val etag: String,
        @Embedded @SerializedName("snippet") val snippet: ChannelSnippet,
        @Embedded @SerializedName("statistics") val statistics: Statistics
) : Parcelable

@Parcelize
data class Statistics(
        @SerializedName("viewCount") val viewCount: String,
        @SerializedName("commentCount") val commentCount: String,
        @SerializedName("subscriberCount") val subscriberCount: String,
        @SerializedName("hiddenSubscriberCount") val hiddenSubscriberCount: Boolean,
        @SerializedName("videoCount") val videoCount: String
) : Parcelable

@Parcelize
data class ChannelSnippet(
        @SerializedName("title") val title: String,
        @SerializedName("description") val description: String,
        @SerializedName("publishedAt") val publishedAt: String,
        @Embedded @SerializedName("thumbnails") val thumbnails: ChannelThumbnails,
        @Embedded @SerializedName("localized") val localized: Localized
) : Parcelable

@Parcelize
data class Localized(
        @SerializedName("title")
        @ColumnInfo(name = "title_localize")
        val title: String,
        @SerializedName("description")
        @ColumnInfo(name = "description_localize")
        val description: String
) : Parcelable

@Parcelize
data class ChannelThumbnails(
        @Embedded @SerializedName("high") val high: ChannelHigh
) : Parcelable

@Parcelize
data class ChannelHigh(
        @SerializedName("url")
        val url: String,
        @SerializedName("width")
        val width: Int,
        @SerializedName("height")
        val height: Int
) : Parcelable