package id.codepresso.kidotube.data.remote

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import id.codepresso.kidotube.data.model.*
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("video_category.json")
    fun getVideoCategory(): Flowable<VideoCategoryResponse>

    @GET("https://www.googleapis.com/youtube/v3/playlistItems")
    fun getVideosByPlaylistId(
            @Query("part") part: String,
            @Query("maxResults") maxResult: Int,
            @Query("playlistId") playlistId: String,
            @Query("key") apiKey: String
    ): Flowable<VideoListResponse>

    @GET("https://www.googleapis.com/youtube/v3/playlistItems")
    fun getNextPageVideosByPlaylistId(
            @Query("part") part: String,
            @Query("maxResults") maxResult: Int,
            @Query("playlistId") playlistId: String,
            @Query("pageToken") pageToken: String,
            @Query("key") apiKey: String
    ): Flowable<VideoListResponse>

    @GET("updater.json")
    fun getUpdater(): Flowable<UpdaterResponse>

    @GET("https://www.googleapis.com/youtube/v3/channels")
    fun getChannels(
            @Query("part") part: String,
            @Query("id") ids: String,
            @Query("key") apiKey: String
    ): Flowable<VideoChannelListResponse>

    @GET("https://www.googleapis.com/youtube/v3/search")
    fun getVideosByChannelId(
            @Query("part") part: String,
            @Query("maxResults") maxResult: Int,
            @Query("channelId") channelId: String,
            @Query("order") order: String,
            @Query("type") type: String,
            @Query("key") apiKey: String
    ): Flowable<VideoListResponse>

    @GET("https://www.googleapis.com/youtube/v3/search")
    fun getNextPageVideosByChannelId(
            @Query("part") part: String,
            @Query("maxResults") maxResult: Int,
            @Query("channelId") channelId: String,
            @Query("pageToken") pageToken: String,
            @Query("order") order: String,
            @Query("type") type: String,
            @Query("key") apiKey: String
    ): Flowable<VideoListResponse>

    @GET("https://www.googleapis.com/youtube/v3/playlists")
    fun getPlaylistByChannelId(
            @Query("part") part: String,
            @Query("maxResults") maxResult: Int,
            @Query("channelId") channelId: String,
            @Query("key") apiKey: String
    ): Flowable<PlaylistListResponse>

    @GET("https://www.googleapis.com/youtube/v3/playlists")
    fun getNextPagePlaylistByChannelId(
            @Query("part") part: String,
            @Query("maxResults") maxResult: Int,
            @Query("channelId") channelId: String,
            @Query("pageToken") pageToken: String,
            @Query("key") apiKey: String
    ): Flowable<PlaylistListResponse>

    companion object Factory {
        var gson: Gson = GsonBuilder()
                .registerTypeAdapter(VideoListResponse::class.java, Deserializer.VideoListDeserializer())
                .create()

        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("http://razibkani.com/app-resources/Muslimedia/")
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}