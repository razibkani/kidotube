package id.codepresso.kidotube.data.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class PlaylistListResponse(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("nextPageToken") val nextPageToken: String,
    @SerializedName("pageInfo") val pageInfo: PlaylistPageInfo,
    @SerializedName("items") val items: List<PlaylistItem>
)

@Parcelize
data class PlaylistPageInfo(
    @SerializedName("totalResults") val totalResults: Int,
    @SerializedName("resultsPerPage") val resultsPerPage: Int
) : Parcelable

@Parcelize
data class PlaylistItem(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("id") val id: String,
    @SerializedName("snippet") val snippet: PlaylistSnippet,
    @SerializedName("contentDetails") val contentDetails: PlaylistContentDetails
) : Parcelable

@Parcelize
data class PlaylistContentDetails(
    @SerializedName("itemCount") val itemCount: Int
) : Parcelable

@Parcelize
data class PlaylistSnippet(
    @SerializedName("publishedAt") val publishedAt: String,
    @SerializedName("channelId") val channelId: String,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("thumbnails") val thumbnails: PlaylistThumbnails,
    @SerializedName("channelTitle") val channelTitle: String,
    @SerializedName("localized") val localized: PlaylistLocalized
) : Parcelable

@Parcelize
data class PlaylistLocalized(
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String
) : Parcelable

@Parcelize
data class PlaylistThumbnails(
    @SerializedName("default") val default: PlaylistDefault,
    @SerializedName("medium") val medium: PlaylistMedium,
    @SerializedName("high") val high: PlaylistHigh
) : Parcelable

@Parcelize
data class PlaylistHigh(
    @SerializedName("url") val url: String,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int
) : Parcelable

@Parcelize
data class PlaylistDefault(
    @SerializedName("url") val url: String,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int
) : Parcelable

@Parcelize
data class PlaylistMedium(
    @SerializedName("url") val url: String,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int
) : Parcelable