package id.codepresso.kidotube.data.local

import android.content.Context
import android.content.SharedPreferences
import id.codepresso.kidotube.util.Config

class PreferencesHelper(context: Context) {

    private val PREF_NAME = Config.PREF_NAME
    private var prefs: SharedPreferences

    object Key {
        const val VIDEO_CATEGORY_MUST_UPDATE = "kidotube_video_category_must_update"
        const val VIDEO_CHANNEL_MUST_UPDATE = "kidotube_video_channel_must_update"

        const val VIDEO_CATEGORY_VERSION = "kidotube_video_category_version"

        const val VIDEO_POSITION = "kidotube_video_position_%s"

        const val INTERSTITIAL_COUNTER = "kidotube_interstitial_counter"
    }

    init {
        prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    fun setBoolean(key: String, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
        return prefs.getBoolean(key, defaultValue)
    }

    fun setString(key: String, value: String) {
        prefs.edit().putString(key, value).apply()
    }

    fun getString(key: String, defaultValue: String = ""): String {
        return prefs.getString(key, defaultValue)
    }

    fun setFloat(key: String, value: Float) {
        prefs.edit().putFloat(key, value).apply()
    }

    fun getFloat(key: String, defaultValue: Float = 0f): Float {
        return prefs.getFloat(key, defaultValue)
    }

    fun setInt(key: String, value: Int) {
        prefs.edit().putInt(key, value).apply()
    }

    fun getInt(key: String, defaultValue: Int = 1): Int {
        return prefs.getInt(key, defaultValue)
    }
}