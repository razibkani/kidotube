package id.codepresso.kidotube.data.local.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "video_category")
@Parcelize
data class VideoCategoryItem(
        @SerializedName("id")
        @PrimaryKey
        val id: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("playlist_id")
        val playlistId: String,
        @SerializedName("thumbnail")
        val thumbnail: String,
        @SerializedName("count_video")
        val videoCount: Int) : Parcelable