package id.codepresso.kidotube.data.model

import com.google.gson.annotations.SerializedName
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem

data class VideoCategoryResponse(@SerializedName("data") val data: List<VideoCategoryItem>)