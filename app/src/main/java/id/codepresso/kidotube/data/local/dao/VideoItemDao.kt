package id.codepresso.kidotube.data.local.dao

import android.arch.persistence.room.*
import id.codepresso.kidotube.data.local.entity.VideoItem
import io.reactivex.Flowable

@Dao
interface VideoItemDao {

    @Query("SELECT * FROM video_item")
    fun getVideosFlowable(): Flowable<List<VideoItem>>

    @Query("SELECT * FROM video_item")
    fun getVideos(): List<VideoItem>

    @Insert
    fun insert(vararg videoItem: VideoItem)

    @Update
    fun update(vararg videoItem: VideoItem)

    @Delete
    fun delete(videoItem: VideoItem)

}