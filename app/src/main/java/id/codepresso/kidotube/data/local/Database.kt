package id.codepresso.kidotube.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import id.codepresso.kidotube.data.local.dao.ChannelItemDao
import id.codepresso.kidotube.data.local.dao.VideoCategoryDao
import id.codepresso.kidotube.data.local.dao.VideoItemDao
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import id.codepresso.kidotube.data.local.entity.VideoItem

@Database(entities = [(VideoCategoryItem::class), (ChannelItem::class), (VideoItem::class)], version = 3, exportSchema = false)
abstract class Database : RoomDatabase() {

    abstract fun videoCategoryDao(): VideoCategoryDao
    abstract fun channelItemDao(): ChannelItemDao
    abstract fun videoItemDao(): VideoItemDao
}