package id.codepresso.kidotube.data.model

import com.google.gson.annotations.SerializedName
import id.codepresso.kidotube.data.local.entity.VideoItem

data class VideoListResponse(
        @SerializedName("kind") val kind: String,
        @SerializedName("etag") val etag: String,
        @SerializedName("nextPageToken") val nextPageToken: String,
        @SerializedName("items") var items: List<VideoItem>
)