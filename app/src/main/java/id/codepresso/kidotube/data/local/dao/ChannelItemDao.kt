package id.codepresso.kidotube.data.local.dao

import android.arch.persistence.room.*
import id.codepresso.kidotube.data.local.entity.ChannelItem
import io.reactivex.Flowable

@Dao
interface ChannelItemDao {

    @Query("SELECT * FROM channel_item")
    fun getChannels(): Flowable<List<ChannelItem>>

    @Insert
    fun insert(vararg channelItem: ChannelItem)

    @Update
    fun update(vararg channelItem: ChannelItem)

    @Delete
    fun delete(channelItem: ChannelItem)

}