package id.codepresso.kidotube.helper

import android.app.Application
import android.content.Context
import com.google.android.gms.ads.*
import id.codepresso.kidotube.BuildConfig
import id.codepresso.kidotube.data.local.PreferencesHelper
import id.codepresso.kidotube.util.Config.ADMOB_ID
import id.codepresso.kidotube.util.Config.BANNER_ID
import id.codepresso.kidotube.util.Config.INTERSTITIAL_ID
import javax.inject.Inject

/**
 * Created by razib on 31/12/2017.
 */
class AdsHelper @Inject constructor(private val mSharedPref: PreferencesHelper) {

    private val mAdRequest: AdRequest = if (BuildConfig.DEBUG) {
        AdRequest.Builder()
                //.addTestDevice("B9AF6EBD0405AD9DFEE7F0D69974C688") // Xiaomi mi5x
                .build()
    } else {
        AdRequest.Builder()
                .build()
    }

    fun loadBanner(context: Context, adsListener: BannerAdsListener?) {
        val adView = AdView(context)
        adView.adSize = AdSize.SMART_BANNER
        adView.adUnitId = BANNER_ID

        adView.adListener = object : AdListener() {
            override fun onAdFailedToLoad(i: Int) {
                adsListener?.onAdsFailedToLoad()
            }

            override fun onAdLoaded() {
                adsListener?.onAdsLoaded(adView)
            }
        }

        adView.loadAd(mAdRequest)
    }

    fun incrementCounter() {
        // Get current value
        val currentValue = mSharedPref.getInt(PreferencesHelper.Key.INTERSTITIAL_COUNTER)
        // Update increment value
        mSharedPref.setInt(PreferencesHelper.Key.INTERSTITIAL_COUNTER, currentValue + 1)
    }

    fun resetInterstitialCounter() {
        mSharedPref.setInt(PreferencesHelper.Key.INTERSTITIAL_COUNTER, 0)
    }

    fun loadInterstitial(context: Context, interstitialAdsListener: InterstitialAdsListener?) {
        val counterValue = mSharedPref.getInt(PreferencesHelper.Key.INTERSTITIAL_COUNTER)
        val interstitialThreshold = 2
        if (counterValue > interstitialThreshold) {
            val interstitialAd = InterstitialAd(context)
            interstitialAd.adUnitId = INTERSTITIAL_ID

            interstitialAd.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    // Reset interstitial counter
                    resetInterstitialCounter()

                    interstitialAdsListener?.onAdsLoaded(interstitialAd)
                }

                override fun onAdFailedToLoad(i: Int) {
                    interstitialAdsListener?.onAdsFailedToLoad()
                }
            }

            interstitialAd.loadAd(mAdRequest)
        }
    }

    interface BannerAdsListener {
        fun onAdsLoaded(adView: AdView)

        fun onAdsFailedToLoad()
    }

    interface InterstitialAdsListener {
        fun onAdsLoaded(interstitialAd: InterstitialAd)

        fun onAdsFailedToLoad()
    }

    companion object {

        fun initAds(application: Application) {
            MobileAds.initialize(application, ADMOB_ID)
        }
    }
}
