package id.codepresso.kidotube.util

interface OnItemFavoriteClickListener<in V> {

    fun onFavorited(item: V)

    fun onUnFavorited(item: V)
}