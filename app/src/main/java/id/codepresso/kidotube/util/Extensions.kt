package id.codepresso.kidotube.util

import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.support.annotation.LayoutRes
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import id.codepresso.kidotube.R

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit, isEnableBackStack: Boolean = true) {
    val fragmentTransaction = beginTransaction()
    if (!isEnableBackStack) {
        popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
    fragmentTransaction.func()
    fragmentTransaction.commit()
}

fun ImageView.loadUrl(url: String, usePlaceholder: Boolean = true) {
    if (usePlaceholder) {
        Picasso.with(this.context).load(url).placeholder(R.drawable.placeholder).into(this)
    } else {
        Picasso.with(this.context).load(url).into(this)
    }
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun Context.shareApplication() {
    val sendIntent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT,
                String.format(Config.MUSLIMEDIA_PLAYSTORE_LINK, this@shareApplication.packageName)
                        .plus("\n\n")
                        .plus(this@shareApplication.getString(R.string.app_label_advertorial)))
        type = "text/plain"
    }
    this.startActivity(Intent.createChooser(sendIntent, this.getString(R.string.share_via)))
}

fun Context.openAppOnPlaystore() {
    val uri = Uri.parse("market://details?id=" + this.packageName)
    val goToMarket = Intent(Intent.ACTION_VIEW, uri).apply {
        // to taken back to our application when backpressed from playstore,
        // we need to add following flags to intent.
        addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
        }
    }
    try {
        this.startActivity(goToMarket)
    } catch (e: ActivityNotFoundException) {
        // Open on browser
        this.startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse(Config.MUSLIMEDIA_PLAYSTORE_LINK)))
    }
}

fun Context.sendSuggestionEmail() {
    val mailto = "mailto:" + Config.EMAIL_ADDRESS +
            "?subject=" + Uri.encode(this.getString(R.string.email_subject))

    val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
        data = Uri.parse(mailto)
    }

    try {
        this.startActivity(emailIntent)
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(this, R.string.no_email_app_is_available, Toast.LENGTH_SHORT).show()
    }
}

fun Context.getVersionName(): String {
    var version = ""
    try {
        val pInfo = this.packageManager.getPackageInfo(this.packageName, 0)
        version = pInfo.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return version
}

fun Context.getVersionCode(): Int {
    var version = 0
    try {
        val pInfo = this.packageManager.getPackageInfo(this.packageName, 0)
        version = pInfo.versionCode
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return version
}

fun Activity.showAboutDialog() {
    val dialog = Dialog(this).apply {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_dialog_about)
        val dialogWindow = window
        if (dialogWindow != null) {
            window!!.setBackgroundDrawable(
                    ColorDrawable(Color.TRANSPARENT))
        }
        show()
    }

    val textVersion = dialog.findViewById<AppCompatTextView>(R.id.text_about_version)
    val btnClose = dialog.findViewById<AppCompatImageView>(R.id.btn_about_close)
    textVersion.text = String.format("v %s", getVersionName())
    btnClose.setOnClickListener { dialog.dismiss() }
}