package id.codepresso.kidotube.util

import android.app.Activity
import android.view.View

class FullScreenManager(private val context: Activity, vararg viewsArg: View) {

    private var views: MutableList<View> = ArrayList()

    init {
        for (item in viewsArg) views.add(item)
    }

    /**
     * call this method to enter full screen
     */
    fun enterFullScreen() {
        val decorView = context.window.decorView

        hideSystemUI(decorView)

        for (view in views) {
            view.visibility = View.GONE
            view.invalidate()
        }
    }

    /**
     * call this method to exit full screen
     */
    fun exitFullScreen() {
        val decorView = context.window.decorView

        showSystemUI(decorView)

        for (view in views) {
            view.visibility = View.VISIBLE
            view.invalidate()
        }
    }

    // hides the system bars.
    private fun hideSystemUI(mDecorView: View) {
        mDecorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    // This snippet shows the system bars.
    private fun showSystemUI(mDecorView: View) {
        mDecorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    }
}