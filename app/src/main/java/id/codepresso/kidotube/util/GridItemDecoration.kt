package id.codepresso.kidotube.util

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class GridItemDecoration(private val spanCount: Int,
                         private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (position >= 0) {
            val column = position % spanCount // item column
            outRect.apply {
                left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    top = spacing
                }
                bottom = spacing // item bottom
            }
        } else {
            outRect.apply {
                left = 0
                right = 0
                top = 0
                bottom = 0
            }
        }
    }
}