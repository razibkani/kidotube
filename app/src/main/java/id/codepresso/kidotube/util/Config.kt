package id.codepresso.kidotube.util

object Config {

    const val YOUTUBE_API_KEY = "AIzaSyDJZoliabIkvdYExOdyUjzzV0SvA9BpL1w"
    const val DB_NAME = "kidotube_db"
    const val PREF_NAME = "kidotube_app_pref"
    const val MUSLIMEDIA_PLAYSTORE_LINK = "https://play.google.com/store/apps/details?id=%s"
    const val EMAIL_ADDRESS = "kanilabs@gmail.com"

    const val ADMOB_ID = "ca-app-pub-4271599463548743~8805866991"
    const val BANNER_ID = "ca-app-pub-4271599463548743/6142801361"
    const val INTERSTITIAL_ID = "ca-app-pub-4271599463548743/2240458645"

    const val ORIGIN_CHANNEL = "channel"
    const val ORIGIN_PLAYLIST = "playlist"
}