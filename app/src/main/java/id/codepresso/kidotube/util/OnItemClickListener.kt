package id.codepresso.kidotube.util

interface OnItemClickListener<in V> {

    fun onClick(item: V)
}