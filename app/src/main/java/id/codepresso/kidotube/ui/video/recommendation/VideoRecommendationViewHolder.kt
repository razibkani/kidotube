package id.codepresso.kidotube.ui.video.recommendation

import android.support.v7.widget.RecyclerView
import android.view.View
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.loadUrl
import kotlinx.android.synthetic.main.item_video_category.view.*

class VideoRecommendationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(videoCategoryItem: VideoCategoryItem,
             onItemClickListener: OnItemClickListener<VideoCategoryItem>?) {

        itemView.ivCategoryThumbnail.loadUrl(videoCategoryItem.thumbnail)
        itemView.tvCategoryName.text = videoCategoryItem.name
        itemView.tvCategoryCount.text = String.format("%d videos", videoCategoryItem.videoCount)

        onItemClickListener?.let {
            itemView.setOnClickListener { onItemClickListener.onClick(videoCategoryItem) }
        }
    }
}