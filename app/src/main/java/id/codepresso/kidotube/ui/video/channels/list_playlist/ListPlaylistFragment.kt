package id.codepresso.kidotube.ui.video.channels.list_playlist

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.data.model.PlaylistItem
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.ui.video.list_video.VideoListFragment
import id.codepresso.kidotube.util.*
import kotlinx.android.synthetic.main.fragment_list_playlist.*
import javax.inject.Inject

class ListPlaylistFragment : Fragment(), ListPlaylistMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var presenter: ListPlaylistPresenter
    @Inject
    lateinit var playlistAdapter: ListPlaylistAdapter

    private lateinit var channelItem: ChannelItem

    companion object {
        val TAG: String = ListPlaylistFragment::class.java.simpleName

        private const val ARG_CHANNEL = "arg_channel"

        fun newInstance(channelItem: ChannelItem): ListPlaylistFragment {
            val args = Bundle()
            args.putParcelable(ARG_CHANNEL, channelItem)
            val fragment = ListPlaylistFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as BaseActivity).activityComponent()?.inject(this)
        arguments?.let {
            channelItem = it.getParcelable(ARG_CHANNEL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter.attachView(this)
        return inflater.inflate(R.layout.fragment_list_playlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        playlistAdapter.onItemClickListener = object : OnItemClickListener<PlaylistItem> {
            override fun onClick(item: PlaylistItem) {
                activity?.supportFragmentManager?.inTransaction ({
                    add(R.id.fragmentContainer, VideoListFragment
                            .newInstance(item.id, item.snippet.title, Config.ORIGIN_PLAYLIST), ListPlaylistFragment.TAG)
                            .addToBackStack(ListPlaylistFragment.TAG)
                }, true)
            }
        }

        rvListPlaylist.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(VerticalItemDecoration(resources.getDimensionPixelSize(R.dimen.divider)))
            playlistAdapter.setHasStableIds(true)
            adapter = playlistAdapter

            addOnScrollListener(object : RecyclerViewScrollListener() {
                override fun onScrollUp() {}
                override fun onScrollDown() {}
                override fun onLoadMore() {
                    loadPlaylists(true)
                }
            })
        }

        srlListPlaylist.setOnRefreshListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (playlistAdapter.playlists.isEmpty()) loadPlaylists(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun loadPlaylists(isLoadMore: Boolean) {
        if (isLoadMore) {
            presenter.getNextPagePlaylistByChannelId(10, channelItem.id)
        } else {
            presenter.getPlaylistByChannelId(10, channelItem.id)
        }
    }

    override fun showLoading() {
        if (playlistAdapter.playlists.isEmpty())
            srlListPlaylist.isRefreshing = true

        playlistAdapter.showLoading = true
    }

    override fun hideLoading() {
        srlListPlaylist.isRefreshing = false
        playlistAdapter.showLoading = false
    }

    override fun enableRefreshing() {
        srlListPlaylist.isEnabled = true
    }

    override fun disableRefreshing() {
        srlListPlaylist.isEnabled = false
    }

    override fun showErrorMessage(throwable: Throwable) {
        throwable.run { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    override fun updateData(listData: List<PlaylistItem>) {
        playlistAdapter.updatePlaylists(listData)
    }

    override fun addData(listData: List<PlaylistItem>) {
        playlistAdapter.addPlaylists(listData)
    }

    override fun onRefresh() {
        loadPlaylists(false)
    }
}
