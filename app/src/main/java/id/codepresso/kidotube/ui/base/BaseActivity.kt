package id.codepresso.kidotube.ui.base

import android.support.v7.app.AppCompatActivity
import id.codepresso.kidotube.KidoTubeApp
import id.codepresso.kidotube.injection.component.ActivityComponent
import id.codepresso.kidotube.injection.component.DaggerActivityComponent
import id.codepresso.kidotube.injection.module.ActivityModule

open class BaseActivity : AppCompatActivity() {

    private var activityComponent: ActivityComponent? = null

    fun activityComponent(): ActivityComponent? {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(ActivityModule(this))
                    .applicationComponent(KidoTubeApp.get(this).getComponent())
                    .build()
        }
        return activityComponent
    }
}