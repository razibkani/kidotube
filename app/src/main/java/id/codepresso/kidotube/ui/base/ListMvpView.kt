package id.codepresso.kidotube.ui.base

interface ListMvpView<in V> : MvpView {

    fun showLoading()

    fun hideLoading()

    fun showErrorMessage(throwable: Throwable)

    fun updateData(listData: List<V>)

    fun enableRefreshing()

    fun disableRefreshing()
}