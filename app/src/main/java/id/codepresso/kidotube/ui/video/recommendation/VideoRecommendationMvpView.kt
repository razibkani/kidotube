package id.codepresso.kidotube.ui.video.recommendation

import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import id.codepresso.kidotube.ui.base.ListMvpView

interface VideoRecommendationMvpView: ListMvpView<VideoCategoryItem> {

    override fun showLoading()

    override fun hideLoading()

    override fun showErrorMessage(throwable: Throwable)

    override fun updateData(listData: List<VideoCategoryItem>)
}