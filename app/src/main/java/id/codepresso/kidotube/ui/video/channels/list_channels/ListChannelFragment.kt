package id.codepresso.kidotube.ui.video.channels.list_channels

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.ui.video.list_video.VideoListFragment
import id.codepresso.kidotube.util.Config
import id.codepresso.kidotube.util.GridItemDecoration
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.inTransaction
import kotlinx.android.synthetic.main.fragment_list_channel.*
import javax.inject.Inject

class ListChannelFragment : Fragment(), ListChannelMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var presenter: ListChannelPresenter
    @Inject
    lateinit var listChannelAdapter: ListChannelAdapter

    companion object {
        val TAG: String = ListChannelFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as BaseActivity).activityComponent()?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list_channel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        listChannelAdapter.onItemClickListener = object : OnItemClickListener<ChannelItem> {
            override fun onClick(item: ChannelItem) {
                activity?.supportFragmentManager?.inTransaction({
                    add(R.id.fragmentContainer, VideoListFragment.newInstance(item.id,
                            item.snippet.title, Config.ORIGIN_CHANNEL), VideoListFragment.TAG)
                            .addToBackStack(VideoListFragment.TAG)
                }, true)
            }
        }

        rvVideoChannel.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(GridItemDecoration(2, resources.getDimensionPixelSize(R.dimen.divider)))
            adapter = listChannelAdapter
        }

        srlVideoChannel.setOnRefreshListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (listChannelAdapter.channelList.isEmpty()) presenter.getChannelList(
                "UC9CsZoTaHZqZjnNZv7v3T1g," +
                        "UC3TT1lmrp5Epv-wNg_h8u-A," +
                        "UCr-rCvgg21KqfrnGopaQeGw," +
                        "UCEmluBS7xbSP4AmEY5dLuGQ," +
                        "UCdknlZNiH-2VWrvjXkx9dJA," +
                        "UCaZFUBmwUqOQKV2H6-5RVUw," +
                        "UC0qrcKuayvbMtPj-WXNFylA," +
                        "UCY2jUnU118sVkdj2xafiJ0g,")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun showLoading() {
        srlVideoChannel.isRefreshing = true
    }

    override fun hideLoading() {
        srlVideoChannel.isRefreshing = false
    }

    override fun showErrorMessage(throwable: Throwable) {
        throwable.run { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    override fun updateData(listData: List<ChannelItem>) {
        listChannelAdapter.updateData(listData)
    }

    override fun enableRefreshing() {
        srlVideoChannel.isEnabled = true
    }

    override fun disableRefreshing() {
        srlVideoChannel.isEnabled = false
    }

    override fun onRefresh() {
        presenter.getChannelList("UC9CsZoTaHZqZjnNZv7v3T1g," +
                "UC3TT1lmrp5Epv-wNg_h8u-A," +
                "UCr-rCvgg21KqfrnGopaQeGw," +
                "UCEmluBS7xbSP4AmEY5dLuGQ," +
                "UCdknlZNiH-2VWrvjXkx9dJA," +
                "UCaZFUBmwUqOQKV2H6-5RVUw," +
                "UC0qrcKuayvbMtPj-WXNFylA," +
                "UCY2jUnU118sVkdj2xafiJ0g,")
    }
}