package id.codepresso.kidotube.ui.video

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.codepresso.kidotube.R
import kotlinx.android.synthetic.main.fragment_video_category.*

class VideoCategoryFragment : Fragment() {

    private lateinit var pagerAdapter: VideoCategoryAdapter

    companion object {
        val TAG: String = VideoCategoryFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pagerAdapter = VideoCategoryAdapter(context, childFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewpager.adapter = pagerAdapter
        tabs.setupWithViewPager(viewpager)
    }
}