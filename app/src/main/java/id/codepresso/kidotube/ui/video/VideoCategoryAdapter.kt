package id.codepresso.kidotube.ui.video

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.codepresso.kidotube.R
import id.codepresso.kidotube.ui.video.channels.list_channels.ListChannelFragment
import id.codepresso.kidotube.ui.video.favorite_video.FavoriteVideoFragment

class VideoCategoryAdapter(private val mContext: Context?, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    // This determines the fragment for each tab
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ListChannelFragment()
            1 -> FavoriteVideoFragment()
            else -> ListChannelFragment()
        }
    }

    // This determines the number of tabs
    override fun getCount(): Int {
        return 2
    }

    // This determines the title for each tab
    override fun getPageTitle(position: Int): CharSequence? {
        // Generate title based on item position
        return when (position) {
            0 -> mContext?.getString(R.string.tab_channels)
            1 -> mContext?.getString(R.string.tab_favorite)
            else -> null
        }
    }

}