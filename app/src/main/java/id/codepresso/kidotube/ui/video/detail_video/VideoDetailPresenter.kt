package id.codepresso.kidotube.ui.video.detail_video

import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.ui.base.BasePresenter
import javax.inject.Inject

class VideoDetailPresenter @Inject constructor(private val dataManager: DataManager) : BasePresenter<VideoDetailMvpView>() {

    fun saveLatestVideoPosition(videoId: String, position: Float) {
        dataManager.saveLatestVideoPosition(videoId, position)
    }

    fun getLatestVideoPosition(videoId: String): Float {
        return dataManager.getLatestVideoPosition(videoId)
    }

    fun isFavoritedVideo(videoItem: VideoItem) : Boolean {
        val favoritedVideo = dataManager.getFavoriteVideos()
        return favoritedVideo.contains(videoItem)
    }

    fun saveToFavorite(videoItem: VideoItem) {
        dataManager.saveFavoriteVideo(videoItem)
    }

    fun removeFromFavorite(videoItem: VideoItem) {
        dataManager.removeFavoriteVideo(videoItem)
    }
}