package id.codepresso.kidotube.ui.video.recommendation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.ui.video.list_video.VideoListFragment
import id.codepresso.kidotube.util.Config
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.VerticalItemDecoration
import id.codepresso.kidotube.util.inTransaction
import kotlinx.android.synthetic.main.fragment_video_recommendation.*
import javax.inject.Inject

class VideoRecommendationFragment : Fragment(), VideoRecommendationMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var presenter: VideoRecommendationPresenter
    @Inject
    lateinit var videoRecommendationAdapter: VideoRecommendationAdapter

    companion object {
        val TAG: String = VideoRecommendationFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as BaseActivity).activityComponent()?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_video_recommendation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        videoRecommendationAdapter.onItemClickListener = object : OnItemClickListener<VideoCategoryItem> {
            override fun onClick(item: VideoCategoryItem) {
                activity?.supportFragmentManager?.inTransaction ({
                    add(R.id.fragmentContainer, VideoListFragment
                            .newInstance(item.playlistId, item.name, Config.ORIGIN_PLAYLIST), VideoRecommendationFragment.TAG)
                            .addToBackStack(VideoRecommendationFragment.TAG)
                }, true)
            }
        }

        rvVideoCategory.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(VerticalItemDecoration(resources.getDimensionPixelSize(R.dimen.divider)))
            adapter = videoRecommendationAdapter
        }

        srlVideoCategory.setOnRefreshListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (videoRecommendationAdapter.videoCategoryList.isEmpty()) presenter.getVideoCategoryList()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun onRefresh() {
        presenter.getVideoCategoryList()
    }

    override fun showLoading() {
        srlVideoCategory.isRefreshing = true
    }

    override fun hideLoading() {
        srlVideoCategory.isRefreshing = false
    }

    override fun enableRefreshing() {
        srlVideoCategory.isEnabled = true
    }

    override fun disableRefreshing() {
        srlVideoCategory.isEnabled = false
    }

    override fun showErrorMessage(throwable: Throwable) {
        throwable.run { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    override fun updateData(listData: List<VideoCategoryItem>) {
        videoRecommendationAdapter.updateData(listData)
    }
}