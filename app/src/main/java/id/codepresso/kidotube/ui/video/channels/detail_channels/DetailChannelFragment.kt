package id.codepresso.kidotube.ui.video

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.codepresso.kidotube.R
import id.codepresso.kidotube.R.id.tabs
import id.codepresso.kidotube.R.id.viewpager
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.ui.homepage.HomepageActivity
import kotlinx.android.synthetic.main.fragment_detail_channel.*

class DetailChannelFragment : Fragment() {

    private lateinit var pagerAdapter: DetailChannelAdapter
    private lateinit var channelItem: ChannelItem

    companion object {
        val TAG: String = DetailChannelFragment::class.java.simpleName

        private const val ARG_CHANNEL_ITEM = "arg_channel_item"

        fun newInstance(channelItem: ChannelItem): DetailChannelFragment {
            val args = Bundle()
            args.putParcelable(ARG_CHANNEL_ITEM, channelItem)
            val fragment = DetailChannelFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { channelItem = it.getParcelable(ARG_CHANNEL_ITEM) }
        pagerAdapter = DetailChannelAdapter(context, channelItem, childFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_channel, container, false)
    }

    override fun onResume() {
        super.onResume()
        initToolbar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewpager.adapter = pagerAdapter
        tabs.setupWithViewPager(viewpager)
    }

    fun initToolbar() {
        ((activity) as HomepageActivity).initToolbar(channelItem.snippet.title, true)
    }
}
