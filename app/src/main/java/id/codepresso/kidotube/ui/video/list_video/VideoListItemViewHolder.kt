package id.codepresso.kidotube.ui.video.list_video

import android.support.v7.widget.RecyclerView
import android.view.View
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.loadUrl
import kotlinx.android.synthetic.main.item_favorite_video.view.*

class VideoListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(videoItem: VideoItem, onItemClickListener: OnItemClickListener<VideoItem>?) {
        itemView.ivVideoThumbnail.loadUrl(videoItem.snippet.thumbnails.medium.url)
        itemView.tvVideoTitle.text = videoItem.snippet.title
        itemView.tvVideoLecture.text = videoItem.snippet.channelTitle

        onItemClickListener?.let {
            itemView.setOnClickListener { onItemClickListener.onClick(videoItem) }
        }
    }
}