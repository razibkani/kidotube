package id.codepresso.kidotube.ui.video.channels.list_playlist

import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.ui.base.BasePresenter
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Inject

class ListPlaylistPresenter @Inject constructor(private val dataManager: DataManager,
                                                private val schedulers: SchedulerProvider) : BasePresenter<ListPlaylistMvpView>() {

    private var nextPageToken: String? = null

    fun getPlaylistByChannelId(maxResult: Int = 10, channelId: String) {
        mvpView?.showLoading()
        dataManager.getPlaylistByChannelId(maxResult, channelId)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ result ->
                    mvpView?.run {
                        nextPageToken = result.nextPageToken
                        hideLoading()
                        updateData(result.items)
                    }
                }, {
                    error ->
                    mvpView?.run {
                        hideLoading()
                        showErrorMessage(error)
                    }
                })
    }

    fun getNextPagePlaylistByChannelId(maxResult: Int = 10, channelId: String) {
        if (nextPageToken == null)
            mvpView?.hideLoading()

        nextPageToken?.let {
            mvpView?.showLoading()
            dataManager.getNextPagePlaylistByChannelId(maxResult, channelId, it)
                    .observeOn(schedulers.ui())
                    .subscribeOn(schedulers.io())
                    .subscribe({ result ->
                        mvpView?.run {
                            nextPageToken = result.nextPageToken
                            hideLoading()
                            addData(result.items)
                        }
                    }, {
                        error ->
                        mvpView?.run {
                            hideLoading()
                            showErrorMessage(error)
                        }
                    })
        }
    }
}