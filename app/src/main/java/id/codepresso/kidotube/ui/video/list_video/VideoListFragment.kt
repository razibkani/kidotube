package id.codepresso.kidotube.ui.video.list_video

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.ads.InterstitialAd
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.ui.homepage.HomepageActivity
import id.codepresso.kidotube.ui.video.detail_video.VideoDetailActivity
import id.codepresso.kidotube.util.Config
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.RecyclerViewScrollListener
import id.codepresso.kidotube.util.VerticalItemDecoration
import kotlinx.android.synthetic.main.fragment_video_list.*
import javax.inject.Inject

class VideoListFragment : Fragment(), VideoListMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var presenter: VideoListPresenter
    @Inject
    lateinit var videoAdapter: VideoListAdapter

    private lateinit var id: String
    private lateinit var title: String
    private lateinit var origin: String

    companion object {
        val TAG: String = VideoListFragment::class.java.simpleName

        private const val ARG_ID = "arg_id"
        private const val ARG_TITLE = "arg_title"
        private const val ARG_ORIGIN = "arg_origin"

        fun newInstance(id: String, title: String, origin: String): VideoListFragment {
            val args = Bundle()
            args.putString(ARG_ID, id)
            args.putString(ARG_TITLE, title)
            args.putString(ARG_ORIGIN, origin)
            val fragment = VideoListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as BaseActivity).activityComponent()?.inject(this)
        arguments?.let {
            id = it.getString(ARG_ID)
            title = it.getString(ARG_TITLE)
            origin = it.getString(ARG_ORIGIN)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter.attachView(this)
        return inflater.inflate(R.layout.fragment_video_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        videoAdapter.onItemClickListener = object : OnItemClickListener<VideoItem> {
            override fun onClick(item: VideoItem) {
                presenter.incrementInterstitialCounter()
                context?.let { VideoDetailActivity.start(it, item) }
            }
        }

        rvVideoList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(VerticalItemDecoration(resources.getDimensionPixelSize(R.dimen.divider)))
            videoAdapter.setHasStableIds(true)
            adapter = videoAdapter

            addOnScrollListener(object : RecyclerViewScrollListener() {
                override fun onScrollUp() {}
                override fun onScrollDown() {}
                override fun onLoadMore() {
                    loadVideoList(true)
                }
            })
        }

        srlVideoList.setOnRefreshListener(this)
    }

    override fun onResume() {
        super.onResume()
        ((activity) as HomepageActivity).initToolbar(title, true)
    }

    override fun onStart() {
        super.onStart()
        if (videoAdapter.videoList.isEmpty()) loadVideoList(false)

        context?.let {
            presenter.loadInterstitial(it, object : AdsHelper.InterstitialAdsListener {
                override fun onAdsLoaded(interstitialAd: InterstitialAd) {
                    interstitialAd.show()
                }

                override fun onAdsFailedToLoad() {}
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun loadVideoList(isLoadMore: Boolean) {
        if (isLoadMore) {
            if (isFromChannel()) {
                presenter.getNextPageVideosByChannelId(10, id)
            } else {
                presenter.getNextPageVideosByPlaylistId(10, id)
            }
        } else {
            if (isFromChannel()) {
                presenter.getVideosByChannelId(10, id)
            } else {
                presenter.getVideosByPlaylistId(10, id)
            }
        }
    }

    override fun showLoading() {
        if (videoAdapter.videoList.isEmpty())
            srlVideoList.isRefreshing = true

        videoAdapter.showLoading = true
    }

    override fun hideLoading() {
        srlVideoList.isRefreshing = false
        videoAdapter.showLoading = false
    }

    override fun enableRefreshing() {
        srlVideoList.isEnabled = true
    }

    override fun disableRefreshing() {
        srlVideoList.isEnabled = false
    }

    override fun showErrorMessage(throwable: Throwable) {
        throwable.run { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    override fun updateData(listData: List<VideoItem>) {
        videoAdapter.updateVideoList(listData)
    }

    override fun addData(listData: List<VideoItem>) {
        videoAdapter.addVideoList(listData)
    }

    override fun onRefresh() {
        loadVideoList(false)
    }

    private fun isFromChannel(): Boolean {
        return origin == Config.ORIGIN_CHANNEL
    }
}