package id.codepresso.kidotube.ui.video.list_video

import android.support.v7.widget.RecyclerView
import android.view.View
import id.codepresso.kidotube.util.hide
import id.codepresso.kidotube.util.visible
import kotlinx.android.synthetic.main.item_footer_loading.view.*

class VideoListLoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(showLoading: Boolean) {
        if (showLoading) itemView.pbFooterLoading.visible() else itemView.pbFooterLoading.hide()
    }
}