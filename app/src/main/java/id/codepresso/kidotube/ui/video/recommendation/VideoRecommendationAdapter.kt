package id.codepresso.kidotube.ui.video.recommendation

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoCategoryItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.inflate
import javax.inject.Inject

class VideoRecommendationAdapter @Inject constructor() : RecyclerView.Adapter<VideoRecommendationViewHolder>() {

    var videoCategoryList: List<VideoCategoryItem> = ArrayList()
    lateinit var onItemClickListener: OnItemClickListener<VideoCategoryItem>

    fun updateData(newVideoCategoryList: List<VideoCategoryItem>) {
        videoCategoryList = newVideoCategoryList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoRecommendationViewHolder {
        val view = parent.inflate(R.layout.item_video_category)
        return VideoRecommendationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return videoCategoryList.size
    }

    override fun onBindViewHolder(holder: VideoRecommendationViewHolder, position: Int) {
        holder.bind(videoCategoryList[position], onItemClickListener)
    }
}