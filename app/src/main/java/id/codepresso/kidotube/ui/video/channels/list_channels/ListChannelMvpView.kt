package id.codepresso.kidotube.ui.video.channels.list_channels

import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.ui.base.ListMvpView

interface ListChannelMvpView : ListMvpView<ChannelItem> {

    override fun showLoading()

    override fun hideLoading()

    override fun showErrorMessage(throwable: Throwable)

    override fun updateData(listData: List<ChannelItem>)

    override fun enableRefreshing()

    override fun disableRefreshing()
}