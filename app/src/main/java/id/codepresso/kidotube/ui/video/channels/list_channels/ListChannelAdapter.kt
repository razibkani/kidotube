package id.codepresso.kidotube.ui.video.channels.list_channels

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.inflate
import javax.inject.Inject

class ListChannelAdapter @Inject constructor() : RecyclerView.Adapter<ListChannelViewHolder>() {

    var channelList: List<ChannelItem> = ArrayList()
    lateinit var onItemClickListener: OnItemClickListener<ChannelItem>

    fun updateData(newChannelList: List<ChannelItem>) {
        channelList = newChannelList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListChannelViewHolder {
        val view = parent.inflate(R.layout.item_video_channel)
        return ListChannelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return channelList.size
    }

    override fun onBindViewHolder(holder: ListChannelViewHolder, position: Int) {
        holder.bind(channelList[position], onItemClickListener)
    }
}