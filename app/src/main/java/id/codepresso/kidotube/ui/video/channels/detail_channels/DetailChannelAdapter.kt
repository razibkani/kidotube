package id.codepresso.kidotube.ui.video

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.ui.video.channels.list_playlist.ListPlaylistFragment
import id.codepresso.kidotube.ui.video.list_video.VideoListFragment
import id.codepresso.kidotube.util.Config

class DetailChannelAdapter(private val mContext: Context?, private val channelItem: ChannelItem, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    // This determines the fragment for each tab
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> VideoListFragment.newInstance(channelItem.id, channelItem.snippet.title, Config.ORIGIN_CHANNEL)
            1 -> ListPlaylistFragment.newInstance(channelItem)
            else -> VideoListFragment.newInstance(channelItem.id, channelItem.snippet.title, Config.ORIGIN_CHANNEL)
        }
    }

    // This determines the number of tabs
    override fun getCount(): Int {
        return 2
    }

    // This determines the title for each tab
    override fun getPageTitle(position: Int): CharSequence? {
        // Generate title based on item position
        return when (position) {
            0 -> mContext?.getString(R.string.tab_videos)
            1 -> mContext?.getString(R.string.tab_playlist)
            else -> null
        }
    }

}