package id.codepresso.kidotube.ui.video.recommendation

import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.ui.base.BasePresenter
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Inject

class VideoRecommendationPresenter @Inject constructor(val dataManager: DataManager,
                                                       val schedulers: SchedulerProvider) : BasePresenter<VideoRecommendationMvpView>() {

    fun getVideoCategoryList() {
        mvpView?.showLoading()
        dataManager.getVideoCategory()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ result ->
                    mvpView?.run {
                        hideLoading()
                        updateData(result)
                        disableRefreshing()
                    }
                }, { error ->
                    mvpView?.run {
                        hideLoading()
                        showErrorMessage(error)
                        enableRefreshing()
                    }
                })
    }
}