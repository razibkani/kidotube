package id.codepresso.kidotube.ui.video.channels.list_channels

import android.support.v7.widget.RecyclerView
import android.view.View
import id.codepresso.kidotube.data.local.entity.ChannelItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.loadUrl
import kotlinx.android.synthetic.main.item_video_channel.view.*

class ListChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: ChannelItem, onItemClickListener: OnItemClickListener<ChannelItem>?) {
        itemView.tvChannelName.text = item.snippet.title
        itemView.ivChannelThumbnail.loadUrl(item.snippet.thumbnails.high.url)

        onItemClickListener?.let {
            itemView.setOnClickListener { onItemClickListener.onClick(item) }
        }
    }
}