package id.codepresso.kidotube.ui.video.channels.list_playlist

import android.support.v7.widget.RecyclerView
import android.view.View
import id.codepresso.kidotube.data.model.PlaylistItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.loadUrl
import kotlinx.android.synthetic.main.item_playlist.view.*

class ListPlaylistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(playlistItem: PlaylistItem,
             onItemClickListener: OnItemClickListener<PlaylistItem>?) {

        itemView.ivPlaylistThumbnail.loadUrl(playlistItem.snippet.thumbnails.medium.url)
        itemView.tvPlaylistName.text = playlistItem.snippet.title
        itemView.tvPlaylistCount.text = String.format("%d videos", playlistItem.contentDetails.itemCount)

        onItemClickListener?.let {
            itemView.setOnClickListener { onItemClickListener.onClick(playlistItem) }
        }
    }
}