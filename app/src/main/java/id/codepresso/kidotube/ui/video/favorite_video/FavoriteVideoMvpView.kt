package id.codepresso.kidotube.ui.video.favorite_video

import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.ui.base.ListMvpView

interface FavoriteVideoMvpView: ListMvpView<VideoItem> {

    override fun showLoading()

    override fun hideLoading()

    override fun showErrorMessage(throwable: Throwable)

    override fun updateData(listData: List<VideoItem>)
}