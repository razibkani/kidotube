package id.codepresso.kidotube.ui.video.channels.list_playlist

import id.codepresso.kidotube.data.model.PlaylistItem
import id.codepresso.kidotube.ui.base.ListMvpView

interface ListPlaylistMvpView : ListMvpView<PlaylistItem> {

    override fun showLoading()

    override fun hideLoading()

    override fun showErrorMessage(throwable: Throwable)

    override fun updateData(listData: List<PlaylistItem>)

    fun addData(listData: List<PlaylistItem>)
}