package id.codepresso.kidotube.ui.video.favorite_video

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.ui.video.detail_video.VideoDetailActivity
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.VerticalItemDecoration
import id.codepresso.kidotube.util.hide
import id.codepresso.kidotube.util.visible
import kotlinx.android.synthetic.main.fragment_favorite_video.*
import kotlinx.android.synthetic.main.layout_empty_favorite.*
import javax.inject.Inject

class FavoriteVideoFragment : Fragment(), FavoriteVideoMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var presenter: FavoriteVideoPresenter
    @Inject
    lateinit var favoriteVideoAdapter: FavoriteVideoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as BaseActivity).activityComponent()?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter.attachView(this)
        return inflater.inflate(R.layout.fragment_favorite_video, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoriteVideoAdapter.onItemClickListener = object : OnItemClickListener<VideoItem> {
            override fun onClick(item: VideoItem) {
                context?.let { VideoDetailActivity.start(it, item) }
            }
        }

        favoriteVideoAdapter.onDeleteFavoriteListener = object : OnItemClickListener<VideoItem> {
            override fun onClick(item: VideoItem) {
                context?.let {
                    AlertDialog.Builder(it)
                            .setTitle(R.string.confirmation)
                            .setMessage(getString(R.string.remove_favorite_confirmation))
                            .setPositiveButton(getString(R.string.delete)) { _, _ -> presenter.deleteFavoriteVideo(item) }
                            .setNegativeButton(getString(R.string.cancel), null)
                            .show()
                }
            }
        }

        rvFavoriteVideoList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(VerticalItemDecoration(resources.getDimensionPixelSize(R.dimen.divider)))
            favoriteVideoAdapter.setHasStableIds(true)
            adapter = favoriteVideoAdapter
        }

        srlFavoriteVideoList.setOnRefreshListener(this)
    }

    override fun onStart() {
        super.onStart()
        loadVideoList()
    }

    override fun showLoading() {
        if (favoriteVideoAdapter.listVideo.isEmpty())
            srlFavoriteVideoList.isRefreshing = true
    }

    override fun hideLoading() {
        srlFavoriteVideoList.isRefreshing = false
    }

    override fun enableRefreshing() {
        srlFavoriteVideoList.isEnabled = true
    }

    override fun disableRefreshing() {
        srlFavoriteVideoList.isEnabled = false
    }

    override fun showErrorMessage(throwable: Throwable) {
        throwable.run { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    override fun updateData(listData: List<VideoItem>) {
        if (listData.isNotEmpty()) {
            llEmptyFavorite.hide()
            rvFavoriteVideoList.visible()
            favoriteVideoAdapter.updateData(listData)
        } else {
            llEmptyFavorite.visible()
            rvFavoriteVideoList.hide()
        }
    }

    override fun onRefresh() {
        loadVideoList()
    }

    private fun loadVideoList() {
        presenter.loadFavoriteVideos()
    }
}
