package id.codepresso.kidotube.ui.video.list_video

import android.content.Context
import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.ui.base.BasePresenter
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Inject

class VideoListPresenter @Inject constructor(private val dataManager: DataManager,
                                             private val adsHelper: AdsHelper,
                                             private val schedulers: SchedulerProvider) : BasePresenter<VideoListMvpView>() {

    private var nextPageToken: String? = null

    fun getVideosByPlaylistId(maxResult: Int = 10, playlistId: String) {
        mvpView?.showLoading()
        dataManager.getVideosByPlaylistId(maxResult, playlistId)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ result ->
                    mvpView?.run {
                        nextPageToken = result.nextPageToken
                        hideLoading()
                        updateData(result.items)
                    }
                }, { error ->
                    mvpView?.run {
                        hideLoading()
                        showErrorMessage(error)
                    }
                })
    }

    fun getVideosByChannelId(maxResult: Int = 10, channelId: String) {
        mvpView?.showLoading()

        dataManager.getVideosByChannelId(channelId, maxResult)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ result ->
                    mvpView?.run {
                        nextPageToken = result.nextPageToken
                        hideLoading()
                        updateData(result.items)
                    }
                }, { error ->
                    mvpView?.run {
                        hideLoading()
                        showErrorMessage(error)
                    }
                })
    }

    fun getNextPageVideosByPlaylistId(maxResult: Int = 10, playlistId: String) {
        if (nextPageToken.isNullOrEmpty())
            mvpView?.hideLoading()

        nextPageToken?.let {
            mvpView?.showLoading()
            dataManager.getNextPageVideosByPlaylistId(maxResult, playlistId, it)
                    .observeOn(schedulers.ui())
                    .subscribeOn(schedulers.io())
                    .subscribe({ result ->
                        mvpView?.run {
                            nextPageToken = result.nextPageToken
                            hideLoading()
                            addData(result.items)
                        }
                    }, { error ->
                        mvpView?.run {
                            hideLoading()
                            showErrorMessage(error)
                        }
                    })
        }
    }

    fun getNextPageVideosByChannelId(maxResult: Int = 10, channelId: String) {
        if (nextPageToken.isNullOrEmpty())
            mvpView?.hideLoading()

        nextPageToken?.let {
            mvpView?.showLoading()
            dataManager.getNextPageVideosByChannelId(channelId, maxResult, it)
                    .observeOn(schedulers.ui())
                    .subscribeOn(schedulers.io())
                    .subscribe({ result ->
                        mvpView?.run {
                            nextPageToken = result.nextPageToken
                            hideLoading()
                            addData(result.items)
                        }
                    }, { error ->
                        mvpView?.run {
                            hideLoading()
                            showErrorMessage(error)
                        }
                    })
        }
    }

    fun incrementInterstitialCounter() {
        adsHelper.incrementCounter()
    }

    fun loadInterstitial(context: Context, interstitialAdsListener: AdsHelper.InterstitialAdsListener) {
        adsHelper.loadInterstitial(context, interstitialAdsListener)
    }
}