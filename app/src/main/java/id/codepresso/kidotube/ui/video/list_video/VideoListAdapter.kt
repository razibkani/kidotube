package id.codepresso.kidotube.ui.video.list_video

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.inflate
import javax.inject.Inject

class VideoListAdapter @Inject constructor() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ITEM = 1
    private val TYPE_LOADING = 2

    var showLoading: Boolean = false
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    var videoList: MutableList<VideoItem> = ArrayList()
    lateinit var onItemClickListener: OnItemClickListener<VideoItem>

    fun updateVideoList(newVideoList: List<VideoItem>) {
        videoList.clear()
        videoList.addAll(newVideoList)
        notifyDataSetChanged()
    }

    fun addVideoList(newVideoList: List<VideoItem>) {
        val lastPosition = videoList.size - 1
        videoList.addAll(newVideoList)
        notifyItemRangeInserted(lastPosition, videoList.size - 1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> VideoListItemViewHolder(parent.inflate(R.layout.item_video))
            else -> VideoListLoadingViewHolder(parent.inflate(R.layout.item_footer_loading))
        }
    }

    override fun getItemCount(): Int {
        // If no items are present, there's no need for loader
        if (videoList.size == 0) {
            return 0
        }

        // +1 for loader
        return if (showLoading) {
            videoList.size + 1
        } else {
            videoList.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        // loader can't be at position 0
        // loader can only be at the last position
        if (isFooter(position)) {
            return TYPE_LOADING
        }
        return TYPE_ITEM
    }

    override fun getItemId(position: Int): Long {
        // loader can't be at position 0
        // loader can only be at the last position
        return if (isFooter(position)) {
            // id of loader is considered as -1 here
            -1
        } else position.toLong()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VideoListItemViewHolder) {
            holder.bind(videoList[position], onItemClickListener)
        } else {
            (holder as VideoListLoadingViewHolder).bind(showLoading)
        }
    }

    private fun isFooter(position: Int): Boolean {
        return position != 0 && position == itemCount - 1 && showLoading
    }
}