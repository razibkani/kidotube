package id.codepresso.kidotube.ui.video.favorite_video

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.inflate
import javax.inject.Inject

class FavoriteVideoAdapter @Inject constructor() : RecyclerView.Adapter<FavoriteVideoViewHolder>() {

    var listVideo: List<VideoItem> = ArrayList()
    lateinit var onItemClickListener: OnItemClickListener<VideoItem>
    lateinit var onDeleteFavoriteListener: OnItemClickListener<VideoItem>

    fun updateData(newListVideo: List<VideoItem>) {
        listVideo = newListVideo
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteVideoViewHolder {
        return FavoriteVideoViewHolder(parent.inflate(R.layout.item_favorite_video))
    }

    override fun getItemCount(): Int {
        return listVideo.size
    }

    override fun onBindViewHolder(holder: FavoriteVideoViewHolder, position: Int) {
        holder.bind(listVideo[position], onItemClickListener, onDeleteFavoriteListener)
    }

}