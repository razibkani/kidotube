package id.codepresso.kidotube.ui.homepage

import android.content.Context
import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.ui.base.BasePresenter
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Inject

class HomepagePresenter @Inject constructor(private val dataManager: DataManager,
                                            private val adsHelper: AdsHelper,
                                            private val schedulers: SchedulerProvider): BasePresenter<HomepageMvpView>() {

    fun getUpdater() {
        dataManager.getUpdater()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({
                    result ->
                    result.version.let {
                        dataManager.setUpdateState(it.videoCategoryVersion)
                    }
                }, {
                    _ ->
                    dataManager.setUpdateState(0) // reset update flag
                })
    }

    fun loadBanner(context: Context, bannerAdsListener: AdsHelper.BannerAdsListener) {
        adsHelper.loadBanner(context, bannerAdsListener)
    }
}