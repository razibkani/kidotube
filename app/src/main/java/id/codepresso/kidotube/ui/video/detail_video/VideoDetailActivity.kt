package id.codepresso.kidotube.ui.video.detail_video

import android.arch.lifecycle.Lifecycle
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerFullScreenListener
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.util.FullScreenManager
import kotlinx.android.synthetic.main.activity_video_detail.*
import javax.inject.Inject

class VideoDetailActivity : BaseActivity(), VideoDetailMvpView {

    val fullScreenManager: FullScreenManager = FullScreenManager(this)

    @Inject
    lateinit var presenter: VideoDetailPresenter
    lateinit var videoItem: VideoItem
    lateinit var videoId: String
    var isFavorited: Boolean = false

    private var videoCurrentPosition: Float = 0f

    companion object {
        private const val ARG_VIDEO_ITEM = "arg_video_item"

        fun start(context: Context, videoItem: VideoItem) {
            val intent = Intent(context, VideoDetailActivity::class.java)
            intent.putExtra(ARG_VIDEO_ITEM, videoItem)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent()?.inject(this)
        setContentView(R.layout.activity_video_detail)
        presenter.attachView(this)

        intent?.extras?.let { videoItem = it.getParcelable(ARG_VIDEO_ITEM) }
        videoItem.videoId?.let {
            videoId = it
        }
        videoItem.snippet.resourceId?.videoId?.let {
            videoId = it
        }

        videoCurrentPosition = presenter.getLatestVideoPosition(videoId)

        initYouTubePlayerView()
        setVideoDescription()
        handleFavoritedVideo()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        ytVideoDetailPlayer.playerUIController.menu?.dismiss()
    }

    override fun onBackPressed() {
        if (ytVideoDetailPlayer.isFullScreen)
            ytVideoDetailPlayer.exitFullScreen()
        else
            super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        presenter.saveLatestVideoPosition(videoId, videoCurrentPosition)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    private fun initYouTubePlayerView() {
        //initPlayerMenu()
        lifecycle.addObserver(ytVideoDetailPlayer)

        ytVideoDetailPlayer.initialize({ youTubePlayer ->
            youTubePlayer.addListener(object : AbstractYouTubePlayerListener() {
                override fun onReady() {
                    loadVideo(youTubePlayer, videoId)
                }

                override fun onCurrentSecond(second: Float) {
                    super.onCurrentSecond(second)
                    videoCurrentPosition = second
                }
            })
            addFullScreenListenerToPlayer()
        }, true)
    }

    private fun setVideoDescription() {
        tvVideoDetailTitle.text = videoItem.snippet.title
        tvVideoDetailLecture.text = videoItem.snippet.channelTitle
        tvVideoDetailDescription.text = videoItem.snippet.description
    }

    /*private fun initPlayerMenu() {
        ytVideoDetailPlayer.playerUIController.showMenuButton(true)
        ytVideoDetailPlayer.playerUIController.menu?.addItem(
                MenuItem("Menu Item 1", R.drawable.ic_menu_24dp, View.OnClickListener {
                    Toast.makeText(this@VideoDetailActivity, "Test", Toast.LENGTH_SHORT).show()
                })
        )
    }*/

    private fun loadVideo(youTubePlayer: YouTubePlayer, videoId: String) {
        if (lifecycle.currentState == Lifecycle.State.RESUMED) {
            youTubePlayer.loadVideo(videoId, videoCurrentPosition)
        } else {
            youTubePlayer.cueVideo(videoId, videoCurrentPosition)
        }

        ytVideoDetailPlayer.playerUIController.setVideoTitle(videoItem.snippet.title)
    }

    private fun addFullScreenListenerToPlayer() {
        ytVideoDetailPlayer.addFullScreenListener(object : YouTubePlayerFullScreenListener {
            override fun onYouTubePlayerEnterFullScreen() {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                fullScreenManager.enterFullScreen()
            }

            override fun onYouTubePlayerExitFullScreen() {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                fullScreenManager.exitFullScreen()
            }
        })
    }

    private fun handleFavoritedVideo() {
        isFavorited = presenter.isFavoritedVideo(videoItem)

        if (isFavorited) {
            tvVideoDetailFavorite.text = getString(R.string.remove_from_favorite)
        } else {
            tvVideoDetailFavorite.text = getString(R.string.save_to_favorite)
        }

        tvVideoDetailFavorite.setOnClickListener {
            if (isFavorited) {
                presenter.removeFromFavorite(videoItem)
                Toast.makeText(this, getString(R.string.remove_favorite), Toast.LENGTH_SHORT).show()
            } else {
                presenter.saveToFavorite(videoItem)
                Toast.makeText(this, getString(R.string.add_favorite), Toast.LENGTH_SHORT).show()
            }
            // Reload
            handleFavoritedVideo()
        }
    }
}