package id.codepresso.kidotube.ui.video.favorite_video

import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.data.local.entity.VideoItem
import id.codepresso.kidotube.ui.base.BasePresenter
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Inject

class FavoriteVideoPresenter @Inject constructor(private val dataManager: DataManager,
                                                 private val schedulers: SchedulerProvider)
    : BasePresenter<FavoriteVideoMvpView>() {

    fun loadFavoriteVideos() {
        mvpView?.showLoading()
        dataManager.getFavoriteVideosFlowable()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ result ->
                    mvpView?.run {
                        hideLoading()
                        updateData(result)
                    }
                }, { error ->
                    mvpView?.run {
                        hideLoading()
                        showErrorMessage(error)
                    }
                })
    }

    fun deleteFavoriteVideo(videoItem: VideoItem) {
        dataManager.removeFavoriteVideo(videoItem)
        loadFavoriteVideos()
    }
}