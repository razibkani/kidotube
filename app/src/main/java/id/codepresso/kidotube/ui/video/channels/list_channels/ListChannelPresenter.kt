package id.codepresso.kidotube.ui.video.channels.list_channels

import id.codepresso.kidotube.data.DataManager
import id.codepresso.kidotube.ui.base.BasePresenter
import id.codepresso.kidotube.util.SchedulerProvider
import javax.inject.Inject

class ListChannelPresenter @Inject constructor(private val dataManager: DataManager,
                                               private val schedulers: SchedulerProvider) : BasePresenter<ListChannelMvpView>() {

    fun getChannelList(ids: String) {
        mvpView?.showLoading()
        dataManager.getChannels(ids)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ result ->
                    mvpView?.run {
                        if (result.isEmpty()) {
                            dataManager.resetChannelUpdateState()
                            getChannelList(ids)
                        } else {
                            hideLoading()
                            updateData(result)
                            disableRefreshing()
                        }
                    }
                }, { error ->
                    mvpView?.run {
                        hideLoading()
                        showErrorMessage(error)
                        enableRefreshing()
                    }
                })
    }
}