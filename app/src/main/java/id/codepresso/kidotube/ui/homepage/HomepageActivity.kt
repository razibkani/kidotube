package id.codepresso.kidotube.ui.homepage

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.ads.AdView
import id.codepresso.kidotube.R
import id.codepresso.kidotube.helper.AdsHelper
import id.codepresso.kidotube.ui.base.BaseActivity
import id.codepresso.kidotube.ui.video.DetailChannelFragment
import id.codepresso.kidotube.ui.video.VideoCategoryFragment
import id.codepresso.kidotube.util.*
import kotlinx.android.synthetic.main.activity_homepage.*
import kotlinx.android.synthetic.main.layout_banner_container.*
import javax.inject.Inject

class HomepageActivity : BaseActivity(), HomepageMvpView {

    @Inject
    lateinit var presenter: HomepagePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityComponent()?.inject(this)

        setContentView(R.layout.activity_homepage)

        initToolbar()

        supportFragmentManager.inTransaction ({
            replace(R.id.fragmentContainer, VideoCategoryFragment(), VideoCategoryFragment.TAG)
        }, false)

        presenter.attachView(this)
        presenter.getUpdater()

        presenter.loadBanner(this, object : AdsHelper.BannerAdsListener {
            override fun onAdsLoaded(adView: AdView) {
                bannerContent.removeAllViews()
                bannerContent.addView(adView)
                bannerContainer.visible()
            }

            override fun onAdsFailedToLoad() {
                bannerContent.removeAllViews()
                bannerContainer.hide()
            }

        })
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.homepage_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_share -> {
                shareApplication()
                true
            }
            R.id.menu_rate_app -> {
                openAppOnPlaystore()
                true
            }
            R.id.menu_send_suggestion -> {
                sendSuggestionEmail()
                true
            }
            R.id.menu_about -> {
                showAboutDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val lastIndex = supportFragmentManager.fragments.size-1
        val fragment = supportFragmentManager.fragments[lastIndex]
        if (fragment is DetailChannelFragment) {
            fragment.initToolbar()
        } else {
            initToolbar()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun initToolbar(toolbarTitle: String = getString(R.string.app_name),
                    isEnable: Boolean = false) {
        toolbar.title = toolbarTitle
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(isEnable)
            setDisplayShowHomeEnabled(isEnable)
        }
    }

}