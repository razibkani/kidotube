package id.codepresso.kidotube.ui.video.channels.list_playlist

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import id.codepresso.kidotube.R
import id.codepresso.kidotube.data.model.PlaylistItem
import id.codepresso.kidotube.ui.video.list_video.VideoListLoadingViewHolder
import id.codepresso.kidotube.util.OnItemClickListener
import id.codepresso.kidotube.util.inflate
import javax.inject.Inject

class ListPlaylistAdapter @Inject constructor() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ITEM = 1
    private val TYPE_LOADING = 2

    var showLoading: Boolean = false
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var playlists: MutableList<PlaylistItem> = ArrayList()
    lateinit var onItemClickListener: OnItemClickListener<PlaylistItem>

    fun updatePlaylists(newPlaylists: List<PlaylistItem>) {
        playlists.clear()
        playlists.addAll(newPlaylists)
        notifyDataSetChanged()
    }

    fun addPlaylists(newPlaylists: List<PlaylistItem>) {
        playlists.addAll(newPlaylists)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ListPlaylistViewHolder(parent.inflate(R.layout.item_playlist))
            else -> VideoListLoadingViewHolder(parent.inflate(R.layout.item_footer_loading))
        }
    }

    override fun getItemCount(): Int {
        // If no items are present, there's no need for loader
        if (playlists.size == 0) {
            return 0
        }

        // +1 for loader
        return if (showLoading) {
            playlists.size + 1
        } else {
            playlists.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        // loader can't be at position 0
        // loader can only be at the last position
        if (isFooter(position)) {
            return TYPE_LOADING
        }
        return TYPE_ITEM
    }

    override fun getItemId(position: Int): Long {
        // loader can't be at position 0
        // loader can only be at the last position
        return if (isFooter(position)) {
            // id of loader is considered as -1 here
            -1
        } else position.toLong()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ListPlaylistViewHolder) {
            holder.bind(playlists[position], onItemClickListener)
        } else {
            (holder as VideoListLoadingViewHolder).bind(showLoading)
        }
    }

    private fun isFooter(position: Int): Boolean {
        return position != 0 && position == itemCount - 1 && showLoading
    }
}